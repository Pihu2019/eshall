//
//  SubscribeViewController.h
//  Eshiksa
//
//  Created by Punit on 05/10/18.
//  Copyright © 2018 Akhilesh. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SubscribeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *amount;
@property (weak, nonatomic) IBOutlet UIButton *subscribeBtn;
- (IBAction)subscribeBtnClicked:(id)sender;


@end

NS_ASSUME_NONNULL_END
