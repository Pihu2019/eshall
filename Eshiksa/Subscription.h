//
//  Subscription.h
//  Eshiksa
//
//  Created by Punit on 05/10/18.
//  Copyright © 2018 Akhilesh. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Subscription : NSObject
@property (nonatomic,strong) NSString *etracki_fees,*maxNoOfBuses,*licenceActivationDate,*licenceDeactivationDate;
@end

NS_ASSUME_NONNULL_END
