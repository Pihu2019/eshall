//
//  ViewLocationViewController.h
//  Eshiksa
//
//  Created by Punit on 09/05/18.
//  Copyright © 2018 Akhilesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewLocationViewController : UIViewController<CLLocationManagerDelegate>
{
CLLocationManager *locationManager;
}
//@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property(nonatomic,retain)NSString *indxp,*journeyIdStr,*stopNameStr,*latitudeStr,*longitudeStr,*addressStr;
@property (weak, nonatomic) IBOutlet UILabel *journeyId;
@property (nonatomic,strong) NSMutableArray *locationArr,*transportArr;

@end
