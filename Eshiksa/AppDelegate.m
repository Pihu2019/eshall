//http://api.shephertz.com/tutorial/Push-Notification-iOS/
//https://www.appcoda.com/firebase-push-notifications/

#import "AppDelegate.h"
#import "ViewController.h"
#import "LoginViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "Firebase.h"
#import "Reachability.h"
#import <unistd.h>
#import "Base.h"
#import "Constant.h"
#import "CircularViewController.h"
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
@import UserNotifications;
#endif
@import Firebase;
@import FirebaseMessaging;
@interface AppDelegate ()<UNUserNotificationCenterDelegate,FIRMessagingDelegate>
#define IDIOM UI_USER_INTERFACE_IDIOM()
#define IPAD UIUserInterfaceIdiomPad
{
    NSString *fcmTokenStr;
    NSString *deviceTokenStr;
}
@end

@implementation AppDelegate
NSString *const kGCMMessageIDKey = @"gcm.message_id";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
      sleep(3);
    
    [FIRApp configure];
    [FIRMessaging messaging].delegate=self;
    application.applicationIconBadgeNumber = 0;
    
    [GMSServices provideAPIKey:@"AIzaSyAAYlz1j5TXbIeO6Ye8lgRbFX0QuR0TKAQ"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyAAYlz1j5TXbIeO6Ye8lgRbFX0QuR0TKAQ"];

    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
    [defaults synchronize];
    
   [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    
    UIStoryboard *storyboard = [self grabStoryboard];
    
    // display storyboard
    self.window.rootViewController = [storyboard instantiateInitialViewController];
    [self.window makeKeyAndVisible];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"username"];
    
    
    if(savedValue!=NULL)
    {
        int screenHeight = [UIScreen mainScreen].bounds.size.height;
        
        
        switch (screenHeight) {
                // iPhone 4s
            case 480:
            {
                storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                LoginViewController *loginController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"identifier"];
                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                self.window.rootViewController = navController;
                
                
                break;
            }
                //iPhone 5 & iPhone SE
            case 548:
            {
                storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                LoginViewController *loginController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"identifier"];
                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                self.window.rootViewController = navController;
                
                
                break;
            }
                // iPhone 5s
            case 568:
            {
                storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                LoginViewController *loginController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"identifier"];
                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                self.window.rootViewController = navController;
                
                
                break;
            }
                // iPhone 6 & 6s & iPhone 7
            case 647:
            {
                storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                LoginViewController *loginController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"identifier"];
                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                self.window.rootViewController = navController;
                
                
                break;
            }
            case 812://iPhone X
            {
                storyboard = [UIStoryboard storyboardWithName:@"Main3" bundle:nil];
                LoginViewController *loginController=[[UIStoryboard storyboardWithName:@"Main3" bundle:nil]instantiateViewControllerWithIdentifier:@"identifier"];
                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                self.window.rootViewController = navController;

                break;
            }
            case 896://iPhone XR,XS Max
            {
                storyboard = [UIStoryboard storyboardWithName:@"Main4" bundle:nil];
                LoginViewController *loginController=[[UIStoryboard storyboardWithName:@"Main4" bundle:nil]instantiateViewControllerWithIdentifier:@"identifier"];
                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                self.window.rootViewController = navController;
                
                break;
            }
            default:
            {
                storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
                LoginViewController *loginController=[[UIStoryboard storyboardWithName:@"Main2" bundle:nil]instantiateViewControllerWithIdentifier:@"identifier"];
                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                self.window.rootViewController = navController;
                
                break;
            }
                
        }
    }

 [self registerForRemoteNotifications];

    return YES;
}
-(void)registerForRemoteNotifications {
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        
        center.delegate = self;
        
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            
            if(!error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }
            
        }];
        
    }
    
    else {
        
        // Code for old versions
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        
                                                        UIUserNotificationTypeBadge |
                                                        
                                                        UIUserNotificationTypeSound);
        
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                
                                                                                 categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    }
    
}
- (UIStoryboard *)grabStoryboard {
    
    // determine screen size
    int screenHeight = [UIScreen mainScreen].bounds.size.height;
    NSLog(@"screenheight==== %d",screenHeight);
    UIStoryboard *storyboard;
    
    switch (screenHeight) {
            // iPhone 4s
        case 480:
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
            //iPhone 5 & iPhone SE
        case 548:
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
            // iPhone 5s
        case 568:
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
            // iPhone 6 & 6s & iPhone 7
        case 647:
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
        case 812://iphone X
            storyboard = [UIStoryboard storyboardWithName:@"Main3" bundle:nil];
            break;
            
        case 896://iphone XR OR iphone XS Max
            storyboard = [UIStoryboard storyboardWithName:@"Main4" bundle:nil];
            break;
            
        default:
            
            storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
            break;
            
    }
    
    return storyboard;
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"recieve new FCM registration token: %@", fcmToken);
    
    [[NSUserDefaults standardUserDefaults] setObject:fcmToken forKey:@"fcmToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [FIRMessaging messaging].APNSToken = deviceToken;//added
    
    NSLog(@"APNs device token retrieved: %@", deviceToken);
    NSString *deviceTokenString=[deviceToken description];
    deviceTokenString=[deviceTokenString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    NSString *str=[deviceTokenString stringByReplacingOccurrencesOfString:@"\\s" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0,[deviceTokenString length])];
    NSLog(@"str==%@",str);
    
    [[NSUserDefaults standardUserDefaults] setValue:str forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"***deviceid***** = %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]);
    
}

//From iOS10, We can display notification when application is in foreground.
//Called when a notification is delivered to a foreground app.

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
    
    [self handleRemoteNotification:[UIApplication sharedApplication] userInfo:notification.request.content.userInfo];
    
}

//Called to let your app know which action was selected by the user for a given notification.

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
    
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    
    [self handleRemoteNotification:[UIApplication sharedApplication] userInfo:response.notification.request.content.userInfo];
    
    completionHandler();
    
}
-(void)handleRemoteNotification:(UIApplication *)application userInfo:(NSDictionary *)remoteNotif{
    
    NSLog(@"handleRemoteNotification");
    
    NSLog(@"Handle Remote Notification Dictionary: %@", remoteNotif);
    
    // Handle Click of the Push Notification From Here…
    // You can write a code to redirect user to specific screen of the app here….

    UIStoryboard *main = [self grabStoryboard];
    CircularViewController *viewConr = [main instantiateViewControllerWithIdentifier:@"CircularViewController"];
    UINavigationController *navCtrl = [[UINavigationController alloc] initWithRootViewController:viewConr];
    self.window.rootViewController=navCtrl;
    [self.window makeKeyAndVisible];
   
  navCtrl.navigationBar.barTintColor =[UIColor colorWithRed:20.0/255.0 green:99.0/255.0 blue:250.0/255.0 alpha:1.0];
  navCtrl.navigationBar.tintColor=[UIColor lightGrayColor];
  navCtrl.navigationBar.titleTextAttributes=@{NSForegroundColorAttributeName:[UIColor whiteColor]};
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    
}
- (void)applicationDidEnterBackground:(UIApplication *)application {
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {

}

- (void)applicationWillTerminate:(UIApplication *)application {

}

@end


