//
//  ChildListViewController.h
//  Eshiksa
//
//  Created by Punit on 29/09/18.
//  Copyright © 2018 Akhilesh. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChildListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property(nonatomic,retain)NSString *indxp,*tag,*success,*error,*user,*childUser,*admissionNumStr,*firstName,*branchIdStr,*addressStr,*emailStr,*lastName,*mobileStr,*opyearStr,*orgIdStr,*picIdStr,*studentIdStr,*transportLicenceStr;
@property (nonatomic,strong) NSMutableArray *childListArr,*admissionNumArr,*firstNameArr;


@end

NS_ASSUME_NONNULL_END
