//
//  SubscribeViewController.m
//  Eshiksa
//
//  Created by Punit on 05/10/18.
//  Copyright © 2018 Akhilesh. All rights reserved.
//

#import "SubscribeViewController.h"
#import "BaseViewController.h"
#import "Base.h"
#import "Subscription.h"
@interface SubscribeViewController ()

@end

@implementation SubscribeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getEtrackiAmount];
    
}

-(void)getEtrackiAmount{
    
    NSString *admissionNum = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"admissionNum"];
    NSLog(@"admissionNum ==%@",admissionNum);
    
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"circular branchid ==%@",branchid);
    
  
    NSString *mystr=[NSString stringWithFormat:@"dbname=%@&Branch_id=%@&admission_no=%@&tag=getEtrackiDetails",dbname,branchid,admissionNum];
    
    NSLog(@"parameterDict in currnt route list%@",mystr);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    
      NSString *mainstr1=[NSString stringWithFormat:@"%@",[subscriptionUrl stringByAppendingString:etracki_payment]];
    
    NSURL *url = [NSURL URLWithString:mainstr1];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[mystr dataUsingEncoding:NSUTF8StringEncoding]];
    NSError *error=nil;
    if(error)
    {
    }
    NSURLSessionDataTask *dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                     {
                                         if(error)
                                         {
                                         }
                                         
                                         NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                         NSError *er=nil;
                                         NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                                         if(er)
                                         {
                                         }
                                         NSLog(@"responseDict:%@",responseDict);
                                         
                                         NSDictionary *dic=[responseDict objectForKey:@"tracking_details"];
                                         NSLog(@"tracking_details dic%@",dic);
                                         
                                         
                                         Subscription *c=[[Subscription alloc]init];
                                         
                                         c.etracki_fees=[dic objectForKey:@"etracki_fees"];
                                         c.maxNoOfBuses=[dic objectForKey:@"maxNoOfBuses"];
                                         c.licenceActivationDate=[dic objectForKey:@"licenceActivationDate"];
                                         c.licenceDeactivationDate=[dic objectForKey:@"licenceDeactivationDate"];
                                        
                                        
                                         NSLog(@" c.etracki_fees==%@ c.maxNoOfBuses ==%@", c.etracki_fees,c.maxNoOfBuses);
                                         
                                         [[NSUserDefaults standardUserDefaults] setObject:c.etracki_fees forKey:@"etrackiFees"];
                                         [[NSUserDefaults standardUserDefaults] synchronize];
                                         
                                     }];
    
    [dataTask resume];
    
}
- (IBAction)subscribeBtnClicked:(id)sender {
    [self getEtrackiSubcription];
}

-(void)getEtrackiSubcription{
    
    NSString *admissionNum = [[NSUserDefaults standardUserDefaults]stringForKey:@"admissionNum"];
    NSLog(@"admissionNum ==%@",admissionNum);
    
    NSString *etrackiFees = [[NSUserDefaults standardUserDefaults]stringForKey:@"etrackiFees"];
    NSLog(@"etrackiFees ==%@",etrackiFees);
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]stringForKey:@"branchid"];
    NSLog(@"circular branchid ==%@",branchid);
    
    
    NSString *mystr=[NSString stringWithFormat:@"dbname=%@&Branch_id=%@&admission_no=%@&amount=%@&tag=getEtrackiDetails",dbname,branchid,admissionNum,etrackiFees];
    
    NSLog(@"parameterDict in currnt route list%@",mystr);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    //NSURL * url = [NSURL URLWithString:@"http://erp.eshiksa.net/eps_trunk/eps/esh/plugins/APIS/etracki_payment.php"];
    
    NSString *mainstr1=[NSString stringWithFormat:@"%@",[subscriptionUrl stringByAppendingString:etracki_payment]];
    
    NSURL *url = [NSURL URLWithString:mainstr1];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[mystr dataUsingEncoding:NSUTF8StringEncoding]];
    NSError *error=nil;
    if(error)
    {
    }
    NSURLSessionDataTask *dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                     {
                                         if(error)
                                         {
                                         }
                                         
                                         NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                         NSError *er=nil;
                                         NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                                         if(er)
                                         {
                                         }
                                         NSLog(@"responseDict:%@",responseDict);
                                         
                                         NSDictionary *dic=[responseDict objectForKey:@"tracking_details"];
                                         NSLog(@"tracking_details dic%@",dic);
                                         
                                         
                                         Subscription *c=[[Subscription alloc]init];
                                         
                                         c.etracki_fees=[dic objectForKey:@"etracki_fees"];
                                         c.maxNoOfBuses=[dic objectForKey:@"maxNoOfBuses"];
                                         c.licenceActivationDate=[dic objectForKey:@"licenceActivationDate"];
                                         c.licenceDeactivationDate=[dic objectForKey:@"licenceDeactivationDate"];
                                         
                                         
                                         NSLog(@" c.etracki_fees==%@ c.maxNoOfBuses ==%@", c.etracki_fees,c.maxNoOfBuses);
                                         
    
                                         
                                     }];
    
    [dataTask resume];
    
}

@end
